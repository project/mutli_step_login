INTRODUCTION
------------
The multi step module allows user to check existence of account.
Most of times when user not sured about their registration.

* First form checks existence of user email or username.
* If user is already registered then login form will come.
* If not registration form will come.

* In second form user can login with email id or username.


REQUIREMENTS
------------

NO special requirements.

INSTALLATION
------------

* Install as you would normally install  
a contributed Drupal module.


CONFIGURATION
------------
This module is providing path - multi-step-form

Example -  example.com/multi-step-form

MAINTAINERS
-----------

Current maintainers:
* Sumit kumar (https://www.drupal.org/u/bsumit5577)
