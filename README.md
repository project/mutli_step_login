##  Multi Step Login module offers following features and usability improvements:
1. First form checks user is already registered in site or not using email or username. If user is already registered then login form will come otherwise user user will redirected to user registeration form.

2. In second form user can login with email id or username.

## How to use:-
* Enable **Multi Step Login Form** module
* Goto: {DOMAIN}/multi-step-form

## Screenshot
![Image of Multi step login form](https://www.drupal.org/files/project-images/Form2.png)