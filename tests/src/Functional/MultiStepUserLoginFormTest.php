<?php

namespace Drupal\Tests\multi_step_login\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Ensure the multi step login functionality works as expected.
 *
 * @group user
 */
class MultiStepUserLoginFormTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['multi_step'];

  /**
   * {@inheritdoc}
   */
  protected $path = 'multi-step-form';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function testUserWillSeeOnlyUserNameOrEmailField() {
    $this->drupalGet($this->path);
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->pageTextContains(t('Email and Username'));
    $this->assertSession()->buttonExists(t('Next'));

    // Ensure password field is not visible.
    $this->assertSession()->pageTextNotContains(t('Enter the password that accompanies your username'));
    $this->assertSession()->buttonNotExists(t('Log in'));
  }

  /**
   * {@inheritdoc}
   */
  public function testUserWillSeePasswordFieldOnlyIfUserNameIsValid() {
    $this->drupalGet($this->path);
    $user = $this->drupalCreateUser([]);
    $edit = ['combo' => $user->getAccountName()];
    $this->submitForm($edit, t('Next'));

    $this->assertSession()->pageTextContains(t('Enter the password that accompanies your username'));
    $this->assertSession()->buttonExists(t('Log in'));
  }

  /**
   * {@inheritdoc}
   */
  public function testUserWillRedirectToRegistrationPageIfUsernameOrEmailDoesNotExists() {
    $this->drupalGet($this->path);
    $edit = ['combo' => 'UserNameNotExists'];
    $this->submitForm($edit, t('Next'));
    $this->assertSession()->addressEquals('user/register');

    $this->assertSession()->pageTextNotContains(t('Enter the password that accompanies your username'));
    $this->assertSession()->buttonNotExists(t('Log in'));
  }

  /**
   * {@inheritdoc}
   */
  public function testUserCanLoginUsingMultiStepForm() {
    $destination = 'the-url-does-not-exists';
    $this->drupalGet($this->path, ['query' => ['destination' => $destination]]);
    $user = $this->drupalCreateUser([]);
    $form = ['combo' => $user->getAccountName()];
    $this->submitForm($form, t('Next'));
    $form = [];
    $form['pass'] = $user->passRaw;
    $this->submitForm($form, t('Log in'));
    $this->drupalGet('/user/' . $user->id());
    $this->assertSession()->statusCodeEquals(200);
  }

}
